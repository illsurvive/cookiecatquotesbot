package main

import (
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/joho/godotenv"
	"gitlab.com/illsurvive/cookiecatquotesbot/internal/app/commands"
	"gitlab.com/illsurvive/cookiecatquotesbot/internal/service/cookiecat"
)

func main() {
	godotenv.Load()
	token := os.Getenv("TOKEN")

	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.UpdateConfig{
		Timeout: 60,
	}

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Panic(err)
	}

	cookieCatService := cookiecat.NewService()

	commander := commands.NewCommander(bot, cookieCatService)

	for update := range updates {
		commander.HandleUpdate(update)
	}
}
