package commands

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/illsurvive/cookiecatquotesbot/internal/service/cookiecat"
)

type Commander struct {
	bot              *tgbotapi.BotAPI
	cookieCatService *cookiecat.Service
}

func NewCommander(
	bot *tgbotapi.BotAPI,
	cookieCatService *cookiecat.Service,
) *Commander {
	return &Commander{
		bot:              bot,
		cookieCatService: cookieCatService,
	}
}
