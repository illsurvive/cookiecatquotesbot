package commands

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Commander) HandleUpdate(update tgbotapi.Update) {
	if update.Message != nil {
		c.handleMessage(update.Message)
	}
}

func (c *Commander) handleMessage(msg *tgbotapi.Message) {
	switch msg.Command() {
	case "quote":
		c.Quote(msg)
	case "sticker":
		c.Sticker(msg)
	default:
		c.Default(msg)
	}
}

func (c *Commander) Default(inputMessage *tgbotapi.Message) {
	c.bot.Send(tgbotapi.NewMessage(inputMessage.Chat.ID, "Печенегр может только отвечать цитатами на команду /quote и стикерами на команду /sticker"))
}
