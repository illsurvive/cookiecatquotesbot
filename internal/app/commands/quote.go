package commands

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Commander) Quote(inputMessage *tgbotapi.Message) {
	quote := c.cookieCatService.GetQuote()
	c.bot.Send(tgbotapi.NewMessage(inputMessage.Chat.ID, quote.Text))
}
