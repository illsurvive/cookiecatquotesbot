package commands

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Commander) Sticker(inputMessage *tgbotapi.Message) {
	sticker := c.cookieCatService.GetSticker()
	c.bot.Send(tgbotapi.NewStickerShare(inputMessage.Chat.ID, sticker.FileId))
}
