package cookiecat

var quotes = []CookieCatQuote{
	{Text: "Every poopoo time is peepee time, but not every peepee time is poopoo time"},
	{Text: "Всё, что мы можем видеть - это сцены из прошлого, никто не знает, что будет завтра"},
	{Text: "Хочешь рассмешить судьбу - расскажи, какого кота ты хочешь"},
	{Text: "Спрашивай правильные вопросы - получай правильные ответы"},
}

var stickers = []CookieCatSticker{
	{FileId: "CAACAgIAAxkBAAEoiaRlj3yJjwO0ShM26Q5fVmK7NQt4XwACCDgAAvFi2UkGx3lcYMx0IjQE"},
	{FileId: "CAACAgIAAxkBAAEoiaZlj3yfUyJ6H9G8RFzgIN7sT4QAAZ0AAlw6AAJvZQFKu-V9_LBEKL40BA"},
	{FileId: "CAACAgIAAxkBAAEoiahlj3yurCaIEXGydn_Z8sbRXFuPJwACwTcAAsVDEUqFm9jqkIxq6zQE"},
	{FileId: "CAACAgIAAxkBAAEoiaplj3zE1Zu4FyAKo4jgpd-EXiLRewAC4zgAArbAEEpdnRGj8NdaLzQE"},
	{FileId: "CAACAgIAAxkBAAEoicZlj3-alteaQ_jM_4drt8LSpULcqwACbkcAAr2AiUpFB_6L9QeLujQE"},
	{FileId: "CAACAgIAAxkBAAEoia5lj3zygI1yCbBnz1H9L764e8xNmAACfToAAkR5qUqxyihWU6ecMTQE"},
	{FileId: "CAACAgIAAxkBAAEoibBlj3z-F95yMS1gVAxl44NFRjHnhAACUjkAAgEI4Ep5kc4zaJXsWjQE"},
	{FileId: "CAACAgIAAxkBAAEoibJlj30Ldc9D1gZyeJMxOwXsqI2QAQAC2jcAAhW2uEvjoza6vBV9BjQE"},
}

type CookieCatQuote struct {
	Text string
}

type CookieCatSticker struct {
	FileId string
}
