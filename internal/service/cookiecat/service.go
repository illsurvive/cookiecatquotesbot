package cookiecat

import (
	"math/rand"
)

type Service struct{}

func NewService() *Service {
	return &Service{}
}

func (s *Service) GetQuote() CookieCatQuote {
	return quotes[rand.Intn(len(quotes))]
}

func (s *Service) GetSticker() CookieCatSticker {
	return stickers[rand.Intn(len(stickers))]
}
